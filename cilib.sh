#!/bin/bash

function init {
  prefixPost="http -a $2:$3 --ignore-stdin -b POST $1/api/v4"
  prefixGet="http -a $2:$3 --ignore-stdin GET $1/api/v4"
  echo "Init CI Lib: $prefixPost"
  result=$($prefixGet/nodes | jq -r '.[].id')
  readarray -t NODES <<<"$result"
  echo "Detect nodes: "
  for i in ${!NODES[*]} ; do
    echo "Node id: ${NODES[$i]}"
  done
}

function repo {
  $prefixPost/repositories name=$1 url=$2
  echo "Repo added: $2"
}

function registry {
  $prefixPost/registries name=$1 url=$2 username=$3 password=$4
  echo "Registry added: $2"
}

function labels {
  node=$1
  labels=$@

  IFS=' ' read -r -a labels <<< "$@"

  for i in ${!labels[*]} ; do
    if [ $i != "0" ]; then
      $prefixPost/nodes/$node/labels key==${labels[$i]} value==true
      echo "Label ${labels[$i]} added to node $node"
    fi
  done
}

function from {
  parts=""
  global_params=()
  parts+="repository==$1 gitRef==$2 TAG=$2 "
  echo "Start deployment from repo: $1:$2..."
}

function fresh {
  parts+="deleteData==$1 "
}

function global_param {
  global_params+=("\"$1\":\"$2\"")
}

function param {
  parts+="$1=$2 "
}

function deploy_group {
  echo "Deploy group: $2:$3..."
  global_params_str=$( IFS=$', '; echo "${global_params[*]}" )
  script="$prefixPost/deploy/group/$1/$2/$3 $parts globalParams:='{$global_params_str}'"
  echo "Deployment script: $script"
  eval $script
}

function deploy_stack {
  echo "Deploy stack: $2:$3..."
  script="$prefixPost/deploy/stack/$1/$2/$3 $parts"
  echo "Deployment script: $script"
  eval $script
}
