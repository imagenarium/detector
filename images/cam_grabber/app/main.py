import os
import shutil
import time
import ffmpeg
import requests
from watchfiles import watch, Change

cam_url = os.environ['CAM_URL']
yns_db = os.environ['YNS_DB']
auth = ('admin', os.environ['YNS_PASSWORD'])


def connect_to_cam():
    resp = requests.get(cam_url)
    resp.encoding = "utf-8-sig"
    print("Подключение к камере: {}".format(cam_url))
    ffmpeg.input(resp.json()["hd_url"]).filter("fps", fps=1/5).output("/tmp/detector/frame_%03d.jpg", **{"qscale:v": 2, "loglevel": "panic"}).run_async()


def process_files():
    requests.put(f'{yns_db}', auth=auth)
    if os.path.isdir("/tmp/detector"):
        shutil.rmtree('/tmp/detector')
    os.mkdir("/tmp/detector")
    for changes in watch("/tmp/detector"):
        for change in changes:
            if change[0] == Change.added:
                file_name = change[1]
                if file_name.endswith(".jpg"):
                    print("Найден новый снимок: " + file_name)
                    with open(file_name, 'rb') as f:
                        resp = requests.post(f'{yns_db}', auth=auth, json={'created': round(time.time())}).json()
                        requests.put(f'{yns_db}/{resp["id"]}/frame?rev={resp["rev"]}', f.read(),
                                     headers={'Content-Type': 'image/jpeg'},
                                     auth=auth)
                    os.remove(file_name)


connect_to_cam()
process_files()
