import base64
import os
import time

import requests

yns_db = os.environ['YNS_DB']
detr_url = os.environ['DETR_URL']
auth = ('admin', os.environ['YNS_PASSWORD'])

design_doc_id = '_design/default'
view_name = 'unprocessed_view'


def create_view():
    design_doc = {
        'views': {
            view_name: {'map': 'function (doc) {if (!doc.processed) emit(doc.created, 1)}'},
            'processed_view': {'map': 'function (doc) {if (doc.processed) emit(doc.created, 1)}'}
        }
    }
    requests.put(f'{yns_db}/{design_doc_id}', auth=auth, json=design_doc)


def process_frames(s, e):
    print(f'Send request start={s}, end={e}')
    response = requests.get(f'{yns_db}/{design_doc_id}/_view/{view_name}?skip_locked=true&start_key={s}&end_key={e}', auth=auth)
    doc_ids = [row['id'] for row in response.json().get('rows', [])]
    print(f'Docs count: {len(doc_ids)}')
    for doc_id in doc_ids:
        print("Process document: " + doc_id)
        doc_url = f'{yns_db}/{doc_id}'
        data = requests.get(f'{doc_url}/frame', auth=auth).content
        detr_response = requests.post(detr_url, files={'file': data}).json()
        names = detr_response["names"]
        doc = requests.get(doc_url, auth=auth).json()
        doc['processed'] = True
        doc['detected_objects'] = names
        rev = requests.put(f'{doc_url}', auth=auth, json=doc).json()['rev']
        if len(names) > 0:
            requests.put(f'{doc_url}/processed_frame?rev={rev}',
                         base64.b64decode(detr_response["image"]),
                         headers={'Content-Type': 'image/jpeg'},
                         auth=auth)


create_view()

start = round(time.time()) - 300
end = start + 10

while True:
    process_frames(start, end)
    start = end - 5
    end = start + 10
    while end > round(time.time()):
        time.sleep(1)
