<@requirement.NODE ref='processor' primary='processor-${namespace}' single='false'  />

<@requirement.PARAM name='TAG' type='tag' scope='global' />
<@requirement.PARAM name='YNS_PASSWORD' value='password' scope='global' />

<@img.TASK 'processor-${namespace}' 'registry.gitlab.com/imagenarium/detector/processor:${PARAMS.TAG}'>
  <@img.NODE_REF 'processor' />
  <@img.ENV 'YNS_DB' 'http://couchdb-proxy-${namespace}/frames' />
  <@img.ENV 'DETR_URL' 'http://detr-${namespace}/detect' />
  <@img.ENV 'YNS_PASSWORD' PARAMS.YNS_PASSWORD />
</@img.TASK>
