<@requirement.NODE 'grabber' 'grabber-${namespace}' 'grabber-standby-${namespace}' />

<@requirement.PARAM name='TAG' type='tag' scope='global' />
<@requirement.PARAM name='CAM_URL' value='https://sochi.camera/vse-kamery/cam-23/?format=json' />
<@requirement.PARAM name='YNS_PASSWORD' value='password' scope='global' />

<@img.TASK 'grabber-${namespace}' 'registry.gitlab.com/imagenarium/detector/grabber:${PARAMS.TAG}'>
  <@img.NODE_REF 'grabber' />
  <@img.ENV 'YNS_DB' 'http://couchdb-proxy-${namespace}/frames' />
  <@img.ENV 'YNS_PASSWORD' PARAMS.YNS_PASSWORD />
  <@img.ENV 'CAM_URL' PARAMS.CAM_URL />
</@img.TASK>
