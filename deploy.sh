#!/bin/bash
source ./cilib.sh
source ./env.sh &> /dev/null || true

init ${IMG_HOST}:5555 ${IMG_USER} ${IMG_PASSWORD}

repo yenisei  https://gitlab.com/imagenarium/yenisei
repo detr     https://gitlab.com/imagenarium/detr
repo detector https://gitlab.com/imagenarium/detector

registry gitlab registry.gitlab.com read_token cSXQYyFK4sx2yhLmbo4Q

for i in ${!NODES[*]} ; do
  labels ${NODES[i]} yns-$((i+1))-$NAMESPACE yns-proxy-$NAMESPACE detr-$NAMESPACE processor-$NAMESPACE
done

labels ${NODES[0]} grabber-$NAMESPACE
labels ${NODES[1]} grabber-standby-$NAMESPACE

from yenisei 2.0
fresh true
global_param YENISEI_PASSWORD $YNS_PASSWORD
deploy_group $NAMESPACE yenisei 2.0

from detr main
deploy_stack $NAMESPACE detr 1.0

from detector $TAG
param YNS_PASSWORD $YNS_PASSWORD
deploy_stack $NAMESPACE grabber latest

from detector $TAG
param YNS_PASSWORD $YNS_PASSWORD
deploy_stack $NAMESPACE processor latest
